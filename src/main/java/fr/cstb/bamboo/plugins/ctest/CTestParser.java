package fr.cstb.bamboo.plugins.ctest;

import com.atlassian.bamboo.configuration.*;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultError;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Sets;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class CTestParser extends DefaultContentHandler
{
    private static final ElementParser DO_NOTHING_PARSER = new DefaultElementParser();

    public static final String NAMESPACE_SEP = "/";

    private String testName;
    private String testMessage;
    private String testTime;
    private boolean isTime = false;
    private boolean isMessage = false;


    private Set<TestResults> failedTests;
    private Set<TestResults> passedTests;

    public CTestParser()
    {
        registerElementParser("Test", new TestParser());
        registerElementParser("FullName", new FullNameParser());
        registerElementParser("NamedMeasurement", new NamedMeasurementParser());
        registerElementParser("Measurement", new MeasurementParser());
        registerElementParser("Value", new ValueParser());
    }

    public Set<TestResults> getFailedTests()
    {
        return failedTests;
    }

    public Set<TestResults> getPassedTests()
    {
        return passedTests;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        //System.out.println(localName);
        if (!hasParserFor(localName))
        {
            registerElementParser(localName, DO_NOTHING_PARSER);
        }
        super.startElement(uri, localName, qName, attributes);
    }

    public void parse(InputStream inputStream) throws IOException, SAXException
    {
        failedTests = Sets.newLinkedHashSet();
        passedTests = Sets.newLinkedHashSet();

        reset();

        XMLReader reader = XMLReaderFactory.createXMLReader(DEFAULT_PARSER);

        reader.setContentHandler(this);
        reader.parse(new InputSource(inputStream));
    }


    class TestParser extends DefaultElementParser
    {
        private boolean isFailed = false;
        private boolean ignore = true;

        @Override
        public void startElement(Attributes attributes)
        {
            if( attributes.getLength() > 0 )
            {
                ignore=false;

                if (attributes.getValue("Status").equalsIgnoreCase("failed"))
                {
                    isFailed = true;
                }
                else
                {
                    isFailed = false;
                }
            }
        }
        @Override
        public void endElement() throws ConfigurationException
        {
            if (!ignore)
            {
                TestResults result = createResult(testName,testTime);
                if(isFailed)
                {
                    result.setState(TestState.FAILED);
                    TestCaseResultError error = new TestCaseResultErrorImpl(testMessage);
                    result.setSystemOut(testMessage);
                    result.addError(error);
                    failedTests.add(result);
                }
                else
                {
                    result.setState(TestState.SUCCESS);
                    result.setSystemOut(testMessage);
                    passedTests.add(result);
                }
                reset();
            }
            else
            {
                ignore = true;
            }
        }
    }

    class FullNameParser extends ElementContentElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            testName = getElementContent();
        }
    }

    class NamedMeasurementParser extends ElementContentElementParser
    {
        @Override
        public void startElement(Attributes attributes)
        {
            if (attributes.getValue("name").equalsIgnoreCase("Execution Time"))
            {
                isTime = true;
            }
        }
        @Override
        public void endElement() throws ConfigurationException
        {
            isTime = false;
        }
    }

    class MeasurementParser extends ElementContentElementParser
    {
        @Override
        public void startElement(Attributes attributes)
        {
            isMessage = true;
        }
        @Override
        public void endElement() throws ConfigurationException
        {
            isMessage = false;
        }

        @Override
        public void characters(char[] chars, int offset, int length) {
            //super.characters(chars, offset, length);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    class ValueParser extends ElementContentElementParser
    {
        @Override
        public void endElement() throws ConfigurationException
        {
            String content = getElementContent();
            if (isTime)
                testTime = content;
            else if (isMessage)
                testMessage = content;
        }
    }

    private void reset()
    {
        testMessage = null;
        testName = null;
        testTime = null;
    }

    private TestResults createResult(String testName, String testTime)
    {
        String className = "";
        String methodName = testName;

        if (StringUtils.contains(testName, NAMESPACE_SEP))
        {
            String[] parts = StringUtils.split(testName, NAMESPACE_SEP);
            if (parts.length > 0)
            {
                methodName = parts[parts.length-1];
                parts = (String[])ArrayUtils.remove(parts, parts.length -1);
                className = StringUtils.join(parts, NAMESPACE_SEP);
            }
        }

        return new TestResults(className, methodName, testTime);
    }
}
